

with mydate as 
(
	select DATEFROMPARTS(year(getdate()), month(getdate()),1) as [the_date]
	union all
	select dateadd(day,1,[the_date])
	from mydate where the_date < eomonth(GETDATE())

)
--- just added a comment here
select * 
from mydate